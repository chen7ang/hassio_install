# 1. hassio_install

hassio 一键安装脚本，实现以下功能。

1. 自动更改系统源为中科大源。（目前支持 Debian Ubuntu Raspbian 三款系统）
2. 自动安装 Docker，可以选择切换 Docker 源为国内源，提高容器下载速度。（注意：Ubuntu 18.10 get.docker.com 官方暂不支持安装docker）
3. 避开 Hassio 因亚马逊连接超时导致无法拉取最新版本的 Homeassistant 容器。

## 1.1. 目前支持的系统

- [Raspbian](https://www.raspberrypi.org/downloads/raspbian/) 
- [Ubuntu](https://www.ubuntu.com/download/server) 测试版本 18LTS通过，但按道理 16 以上都可以。
- [Debian](https://www.debian.org/distrib/netinst) 测试版本 >=9.5 通过。

## 1.2. 使用方法

以 root 身份运行以下命令。

```bash
wget https://gitee.com/Chen7ang/hassio_install/raw/master/install.sh
chmod a+x install.sh
./install.sh
```

### 1.2.1. 如果安装的是 64 位系统，脚本会自动筛选适配 64 位的设备列表

```
(1). 是否将系统源切换为中科大(USTC)源（目前支持 Debian Ubuntu Raspbian 三款系统）
请输入 y or n（默认 yes):y

(2).是否更新系统软件到最新？
[warn] 如果系统依赖版本低于 supervisor 要求,会导致 supervisor 显示系统不健康,最终导致无法安装 addons.
请输入 yes 或者 no（默认：no）：

(3). 找到该系统中有以下用户名
如下方列表未显示你的用户名，请切换回你用户账号后输入 sudo usermod -aG docker $USER 添加用户到 docker 用户组。
    [1]: nero
    [2]: systemd-coredump
    [s]: 跳过
请输入你需要使用 docker 的用户名序号，以加入 docker 用户组:1
将wenjie用户添加至 docker 用户组。


(4).是否需要替换 docker 默认源？
请输入 yes 或者 no（默认：yes）：y


(5).请选择你设备类型（默认：qemux86-64）
    [1]: intel-nuc: 英特尔的nuc小主机
    [2]: odroid-c2: 韩国odroid-c2
    [3]: odroid-xu: 韩国odroid-xu
    [4]: orangepi-prime: 香橙派
    [5]: qemuarm-64: 通用arm设备（例如斐讯N1) 64位系统
    [6]: qemux86-64: 通用X86（普通的PC机电脑）64位系统
    [7]: raspberrypi3-64: 树莓派三代64位系统
    [8]: raspberrypi4-64: 树莓派四代64位系统
    [9]: tinker: 华硕tinker
输入数字 (1-9):6
你选择了 qemux86-64

(6).是否需要设置 hassio 数据保存路径（默认：/usr/share/hassio）
请输入 yes 或 no (默认：no）:
hassio 数据路径为默认路径: /usr/share/hassio

 ################################################################################
 # 1. 是否将系统源切换为清华源:         是
 # 2. 是否更新系统软件到最新:           否
 # 3. 是否将用户添加至 Docker 用户组:   是,添加用户为 nero
 # 4. 是否将 Docker 源切换至国内源:     是，切换源选择：
 # 5. 您的设备类型为:                   qemux86-64
 # 6. 您的 hassio 数据路径为:           /usr/share/hassio
 ################################################################################
请确认以上信息，继续请按任意键，如需修改请输入 Ctrl+C 结束任务重新执行脚本。
```

### 1.2.2. 设备类型选型说明
- intel-nuc ：英特尔的nuc小主机
- odroid-c2 ：韩国odroid-c2
- odroid-xu ：韩国odroid-xu
- orangepi-prime ：香橙派
- qemuarm ：通用arm设备（例如斐讯N1)
- qemuarm-64 ：通用arm设备（例如斐讯N1) 64位系统
- qemux86 ：通用X86 64位系统（普通的PC机电脑）
- qemux86-64 ：通用X86（普通的PC机电脑）64位系统
- raspberrypi ：树莓派一代
- raspberrypi2 ：树莓派二代
- raspberrypi3 ：树莓派三代
- raspberrypi4 ：树莓派四代
- raspberrypi3-64 ：树莓派三代64位系统
- raspberrypi4-64 ：树莓派四代64位系统
- tinker ：华硕tinker

# 2. Hassio 升级脚本

由于网络原因，hassio_supervisor 经常如下报告:

```bash
18-08-15 03:57:02 WARNING (MainThread) [hassio.updater] Can't fetch versions from https://s3.amazonaws.com/hassio-version/stable.json
```

无法从亚马逊刷新最新版的 json 文件，导致 hassio 升级失败，我制作了 hassio 升级脚本，可以使用脚本手动升级。

## 2.1. 使用方法

请使用 root 权限运行以下命令。

```bash
wget https://gitee.com/Chen7ang/hassio_install/raw/master/hassio_upgrade.sh
chmod u+x hassio_upgrade.sh
./hassio_upgrade.sh
```

# 3. HA 版本切换脚本

此脚本可在宿主中切换homeassistant版本号

## 3.1. 严重警告
1. 切换版本的 home-assistant 请先备份好配置文件，虽然脚本会自动备份，但最好自己再备份一次，出现丢失配置情况恕不负责。
2. 切换旧版本启动失败的，请查看 home-assistant 的日志来修复错误配置
3. 切换过旧的版本会导致 hassio 加载 404，目前已知 0.77 以前版本都无法正常加载 hassio
4. 启动失败可以到论坛带日志发帖求助，无日志发帖我将会扣分处理

## 3.2. 使用方法

使用 root 运行一下命令

```
wget https://gitee.com/Chen7ang/hassio_install/raw/master/homeassistant_ver_switch.sh
chmod u+x homeassistant_ver_switch.sh
./homeassistant_ver_switch.sh 0.92.2
```

# 4. 删除系统脚本

此脚本清理删除安装的Supervisor及HA相关软件

## 4.1. 严重警告
1. 本脚本会删除所有HA的组件及本机配置，无法恢复，无法恢复，无法恢复！！
2. 使用后果自负
## 4.2. 使用方法

使用 root 运行一下命令

```
wget https://gitee.com/Chen7ang/hassio_install/raw/master/hassio_del.sh
chmod u+x hassio_del.sh
./hassio_del.sh
```

# 5. 常用命令
## 5.1. 服务控制操作说明

1. 停止（但重启依然会自启动） `systemctl stop hassio-supervisor.service`
2. 重启 `systemctl restart hassio-supervisor.service`
3. 禁用自启动`systemctl disable hassio-supervisor.service`
4. 启用自启动 `systemctl enable hassio-supervisor.service`
5. 查询当前启动状态 `systemctl status hassio-supervisor.service`
6. 查询当前是否自启动`systemctl  is-enabled hassio-supervisor.service`
7. 查询 hassio 日志 `docker logs -f hassio_supervisor`
8. 查询 hassio 日志最新20行信息 `docker logs -f hassio_supervisor --tail 20`
9. 查询 ha 日志 `docker logs -f homeassistant`
10. 查询 ha 日志最新20行信息 `docker logs -f homeassistant --tail 20`

## 参考
1. systemctl 说明 ： [https://linux.cn/article-5926-1.html](https://linux.cn/article-5926-1.html)
2. docker logs 命令用法：[https://docs.docker.com/engine/reference/commandline/logs](https://docs.docker.com/engine/reference/commandline/logs)