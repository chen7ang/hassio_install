#!/bin/bash

# Author : for5million
# Email : for5million@gmail.com
# Version : 1.0
# Date : 2021-09-08

function info { echo -e "\e[32m[info] $*\e[39m"; }

del_hassio(){
	systemctl stop hassio-supervisor.service
	systemctl disable hassio-supervisor.service
	docker stop $(docker ps -q -a --filter name=homeassistant --filter name=hassio_ --filter name=addon_)
	docker stop $(docker ps -q -a --filter name=homeassistant --filter name=hassio_ --filter name=addon_)
	docker rm $(docker ps -q -a --filter name=homeassistant --filter name=hassio_ --filter name=addon_)
	rm -fr /usr/share/hassio
}

# Main
echo -e "是否将系统完全删除----------删除不可恢复！！！！！！"
echo -e "是否将系统完全删除----------删除不可恢复！！！！！！"
echo -e "是否将系统完全删除----------删除不可恢复！！！！！！"
while true; do
    read -p "请输入 y or n（默认 no):" selected
    case ${selected} in
        yes|y|YES|Y|Yes )
            del=true
            break;
            ;;
        ''|no|n|NO|N|No)
            del=false
            break;
            ;;
        *)
            echo "输入错误，请重新输入。"
            ;;
    esac
done

if  [[ ${del} == true ]]; then
    info "删除系统中....."
    del_hassio
else
    info "谢谢你没有删除我。。。。"
	exit 0
fi

